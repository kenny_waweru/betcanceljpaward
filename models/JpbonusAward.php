<?php

namespace frontend\models;

use Yii;
use frontend\models\JackpotEvent;

/**
 * This is the model class for table "jpbonus_award".
 *
 * @property integer $id
 * @property integer $jackpot_event_id
 * @property double $jackpot_bonus
 * @property integer $betika_points_bonus
 * @property string $created_by
 * @property string $created
 * @property string $modified
 */
class JpbonusAward extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jpbonus_award';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jackpot_event_id', 'jackpot_bonus', 'betika_points_bonus', 'created_by'], 'required'],
            [['jackpot_event_id', 'betika_points_bonus'], 'integer'],
            [['jackpot_bonus'], 'number'],
            [['created', 'modified'], 'safe'],
            [['created_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jackpot_event_id' => 'Jackpot Event ID',
            'jackpot_bonus' => 'Jackpot Bonus',
            'betika_points_bonus' => 'Betika Points Bonus',
            'created_by' => 'Created By',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJackpotEvents()
    {
        return $this->hasMany(JackpotEvent::className(), ['jackpot_event' => 'jackpot_event_id']);
    }

     public function awardJackpotBonus($jackpot_event_id, $jackpot_bonus, $betika_points_bonus, $created_by) {
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
               
                $sql = "INSERT INTO jpbonus_award(jackpot_event_id,jackpot_bonus,betika_points_bonus,created_by)  VALUES('$jackpot_event_id','$jackpot_bonus','$betika_points_bonus','$created_by')";
                
                $connection->createCommand($sql)->execute();

                $transaction->commit();
                
                return TRUE;
        
        } catch (Exception $exc) {
            $transaction->rollback();
            return FALSE;
        }
        
    }
}
