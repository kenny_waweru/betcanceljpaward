<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "jackpot_type".
 *
 * @property integer $jackpot_type_id
 * @property string $name
 * @property integer $sub_type_id
 *
 * @property JackpotEvent[] $jackpotEvents
 */
class JackpotType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jackpot_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sub_type_id'], 'required'],
            [['sub_type_id'], 'integer'],
            [['name'], 'string', 'max' => 250],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jackpot_type_id' => 'Jackpot Type ID',
            'name' => 'Name',
            'sub_type_id' => 'Sub Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJackpotEvents()
    {
        return $this->hasMany(JackpotEvent::className(), ['jackpot_type' => 'jackpot_type_id']);
    }
}
