<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "bet".
 *
 * @property integer $bet_id
 * @property integer $profile_id
 * @property string $bet_message
 * @property string $total_odd
 * @property string $bet_amount
 * @property string $possible_win
 * @property integer $status
 * @property integer $win
 * @property string $reference
 * @property string $created_by
 * @property string $created
 * @property string $modified
 *
 * @property Profile $profile
 * @property BetSlip[] $betSlips
 * @property BonusBet[] $bonusBets
 * @property JackpotBet[] $jackpotBets
 * @property JackpotWinner[] $jackpotWinners
 * @property Winner[] $winners
 */
class Bet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id', 'bet_message', 'total_odd', 'bet_amount', 'possible_win', 'status', 'reference', 'created_by', 'created', 'modified'], 'required'],
            [['profile_id', 'status', 'win'], 'integer'],
            [['bet_message'], 'string'],
            [['total_odd', 'bet_amount', 'possible_win'], 'number'],
            [['created', 'modified'], 'safe'],
            [['reference', 'created_by'], 'string', 'max' => 70],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'profile_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bet_id' => 'Bet ID',
            'profile_id' => 'Profile ID',
            'bet_message' => 'Bet Message',
            'total_odd' => 'Total Odd',
            'bet_amount' => 'Bet Amount',
            'possible_win' => 'Possible Win',
            'status' => 'Status',
            'win' => 'Win',
            'reference' => 'Reference',
            'created_by' => 'Created By',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['profile_id' => 'profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBetSlips()
    {
        return $this->hasMany(BetSlip::className(), ['bet_id' => 'bet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusBets()
    {
        return $this->hasMany(BonusBet::className(), ['bet_id' => 'bet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJackpotBets()
    {
        return $this->hasMany(JackpotBet::className(), ['bet_id' => 'bet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJackpotWinners()
    {
        return $this->hasMany(JackpotWinner::className(), ['bet_id' => 'bet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWinners()
    {
        return $this->hasMany(Winner::className(), ['bet_id' => 'bet_id']);
    }
}
