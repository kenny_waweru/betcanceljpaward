<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\JackpotEvent;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\JpbonusAward */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jpbonus-award-form">

    <?php $form = ActiveForm::begin(); ?>
    <label>Jackpot Event</label>
    <?= Html::activeDropDownList($model, 'jackpot_event_id',
      ArrayHelper::map(JackpotEvent::find()->all(), 'jackpot_event_id', 'jackpot_name'), ['class'=>'form-control', 'label'=>'Jackpot Eent']) ?>
     <br>

    <?= $form->field($model, 'jackpot_bonus')->textInput() ?>

    <?= $form->field($model, 'betika_points_bonus')->textInput() ?>

    <?= $form->field($model, 'created_by')->hiddenInput(['value' => 'Ronoh'])->label(false); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Award Bonus' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
