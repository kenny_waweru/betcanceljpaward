<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\models\JackpotEvent;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\JpbonusAwardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jackpot Bonus Awards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jpbonus-award-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Jackpot Bonus Award', ['create'], ['class' => 'btn btn-success']) ?>
        <a class="btn btn-info" href="<?php echo Url::to(['jpbonus-award/awardjackpotbonus']) ?>">Manually Award Jackpot Bonus</a>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'jackpot_event_id',
            'jackpot_bonus',
            'betika_points_bonus',
            'created_by',
            'created',
            'modified',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
