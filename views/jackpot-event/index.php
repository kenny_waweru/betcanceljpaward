<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\JackpotEventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jackpot Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jackpot-event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Jackpot Event', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'jackpot_event_id',
            'jackpot_type',
            'jackpot_name',
            'created_by',
            'status',
            // 'bet_amount',
            // 'total_games',
            // 'created',
            // 'modified',
            // 'jackpot_amount',
            // 'requisite_wins',
            // 'jp_key',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
