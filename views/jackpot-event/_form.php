<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\JackpotEvent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jackpot-event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jackpot_type')->textInput() ?>

    <?= $form->field($model, 'jackpot_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'CANCELLED' => 'CANCELLED', 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', 'SUSPENDED' => 'SUSPENDED', 'FINISHED' => 'FINISHED', 'OTHER' => 'OTHER', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'bet_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_games')->textInput() ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'modified')->textInput() ?>

    <?= $form->field($model, 'jackpot_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'requisite_wins')->textInput() ?>

    <?= $form->field($model, 'jp_key')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
